﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddEditAviokompanija.xaml
    /// </summary>
    public partial class AddEditAviokompanija : Window
    {
        public enum Opcija { DODAVANJE, IZMENA }
        Aviokompanija aviokompanija;
  



        Opcija opcija;

        public AddEditAviokompanija(Aviokompanija aviokompanija, Opcija opcija)
        {

            InitializeComponent();
            this.aviokompanija = aviokompanija;
            this.opcija = opcija;

            this.DataContext = aviokompanija;

            TxtSifra.Text = aviokompanija.Sifra;
            TxtNaziv.Text = aviokompanija.Naziv;
            


            if (opcija.Equals(Opcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;
                foreach (var let in Data.Instance.Letovi)
                {
                    if (aviokompanija.Sifra.Equals(let.AvioKompanija))
                    {
                        ListaLetova.Items.Add(let.SifraLeta);
                    }
                }
            }

        }
        private bool postojiAviokompanija(string sifra)
        {
            foreach (var aerodrom in Data.Instance.Aviokompanije)
            {
                if (aviokompanija.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (opcija.Equals(Opcija.DODAVANJE))
            {
                foreach (var aviok2 in Data.Instance.Aviokompanije)
                {
                    if (TxtSifra.Text.Equals(aviok2.Sifra))
                    {
                        MessageBox.Show("Vec postoji Aviokompanija sa tom sifrom sifra mora biti jedinstvena");
                    }
                }
                if (string.IsNullOrEmpty(TxtNaziv.Text) || string.IsNullOrEmpty(TxtSifra.Text))
                {
                    MessageBox.Show("Niste popunili sva polja");
                }
            }


            if (opcija.Equals(Opcija.IZMENA)) {


                if (string.IsNullOrEmpty(TxtNaziv.Text) || string.IsNullOrEmpty(TxtSifra.Text))
                {
                    MessageBox.Show("Niste popunili sva polja");
                }
                else
                {
                    
                    this.DialogResult = true;
                    Aviokompanija.IzmeniAviokompanije(aviokompanija);
                    this.Close();

                }
            }

            else if (opcija.Equals(Opcija.DODAVANJE))
            {
                if (string.IsNullOrEmpty(TxtNaziv.Text) || string.IsNullOrEmpty(TxtSifra.Text))
                {
                    MessageBox.Show("Niste popunili sva polja");
                }
                else
                {
                    Data.Instance.Aviokompanije.Add(aviokompanija);
                    MessageBox.Show("Uspesno ste sacuvali aviokompanija");
                    this.DialogResult = true;
                    Aviokompanija.DodajAviokompaniju(aviokompanija);
                    this.Close();
                }
            }


        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
    }
