﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{

    public partial class AddEditAvion : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Avion avion;

        public AddEditAvion(Avion avion,EOpcija opcija)
        {

            InitializeComponent();
            this.avion = avion;
            this.opcija = opcija;

            cbAviokompanija.ItemsSource = Data.Instance.Aviokompanije.Select(a => a.Sifra);
            



            this.DataContext = avion;

            if (opcija.Equals(EOpcija.IZMENA))
            {

                TxtSifra.IsEnabled = false;

            }
            /*
            cbAviokompanija.SelectedItem = avion.NazivAvioKompanije;
            TxtSifra.Text = avion.Sifra;
            TxtBrojLeta.Text = avion.BrojLeta;
            TxtSedistaBiz.Text = avion.SedistaBiz;
            TxtSedistaEko.Text = avion.SedistaEko;*/
        }

        private bool AllreadyExistAirport(string sifra)
        {
            foreach (var a in Data.Instance.Avioni)
            {
                if (a.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
        private bool AllreadyExistAvion(string sifra)
        {
            foreach (var a in Data.Instance.Avioni)
            {
                if (a.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
                         if (opcija.Equals(EOpcija.DODAVANJE))
            {
                foreach (var avio in Data.Instance.Avioni)
                {
                    if (TxtSifra.Text.Equals(avio.Sifra))
                    {
                        MessageBox.Show("Vec postoji aerodrom sa tom sifrom sifra mora biti jedinstvena");
                    }
                }
            }

            if (string.IsNullOrEmpty(TxtSifra.Text) || string.IsNullOrEmpty(TxtSedistaEko.Text) || string.IsNullOrEmpty(TxtSedistaBiz.Text) || string.IsNullOrEmpty(TxtBrojLeta.Text))
            {
                MessageBox.Show("Niste popunili sva polja");
            }
            else if (TxtSifra.Text.Length > 4)
            {
                MessageBox.Show("Sifra ne moze imati vise od 4 karaktera");
            }



            else
            {
                if (opcija.Equals(EOpcija.IZMENA))
                {
                    cbAviokompanija.SelectedItem = avion.NazivAvioKompanije;
                    this.DialogResult = true;
                    Avion.IzmeniAvion(avion);
                    this.Close();
                }

                else if (opcija.Equals(EOpcija.DODAVANJE))
                {
                    Data.Instance.Avioni.Add(avion);
                    MessageBox.Show("Uspesno ste sacuvali");
                    cbAviokompanija.SelectedItem = avion.NazivAvioKompanije;
                    this.DialogResult = true;
                    Avion.DodajAvion(avion);
                    this.Close();
                }
            }

        }

        private void BtnPonisti_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

