﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddNEditWindow.xaml
    /// </summary>
    public partial class AddNEditWindow : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Aerodrom aerodrom;
        public AddNEditWindow(Aerodrom aerodrom, EOpcija opcija)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.opcija = opcija;

            this.DataContext = aerodrom;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;

            }
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (opcija.Equals(EOpcija.DODAVANJE)){
                foreach (var aerodrom2 in Data.Instance.Aerodromi)
                {
                    if (TxtSifra.Text.Equals(aerodrom2.Sifra))
                    {
                        MessageBox.Show("Vec postoji aerodrom sa tom sifrom sifra mora biti jedinstvena");
                    }
                }
            }
        
            if (string.IsNullOrEmpty(TxtNaziv.Text) || string.IsNullOrEmpty(TxtSifra.Text) || string.IsNullOrEmpty(TxtGrad.Text))
            {
                MessageBox.Show("Niste popunili sva polja");
            }
            else if (TxtSifra.Text.Length > 3)
            {
                MessageBox.Show("Sifra ne moze imati vise od 3 karaktera");
            }
            else
            {
                if (opcija.Equals(EOpcija.IZMENA))
                {
                    MessageBox.Show("Uspesno sacuvano");
                    this.DialogResult = true;
                    Aerodrom.IzmeniAerodrom(aerodrom);

                    this.Close();
                }
                else if (opcija.Equals(EOpcija.DODAVANJE))
                {
                    Data.Instance.Aerodromi.Add(aerodrom);
                    MessageBox.Show("Uspesno sacuvano");
                    this.DialogResult = true;
                    Aerodrom.DodajAerodrom(aerodrom);

                    this.Close();
                }

            }
        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        private bool AllreadyExistAirport(string sifra)
        {
            foreach (var a in Data.Instance.Aerodromi)
            {
                if (a.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
