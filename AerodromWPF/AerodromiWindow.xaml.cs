﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AerodromiWindow.xaml
    /// </summary>
    public partial class AerodromiWindow : Window
    {
        ICollectionView view;
        ICollectionView view2;
        public AerodromiWindow()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Data.Instance.Aerodromi);
            DGAerodromi.ItemsSource = view;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;
           

           


        }
   

        private void BtnDodajAerodrom_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = new Aerodrom();
            // TODO populate airport data
            AddNEditWindow eaw = new AddNEditWindow(aerodrom, AddNEditWindow.EOpcija.DODAVANJE);
            eaw.ShowDialog();

        }

        private void BtnIzmeniAerodrom_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindow eaw;
            Aerodrom selektovani = view.CurrentItem as Aerodrom;
            if (SelektovanAerodrom(selektovani))
            {
                Aerodrom oldAirport = (Aerodrom)selektovani.Clone();
                eaw = new AddNEditWindow(selektovani, AddNEditWindow.EOpcija.IZMENA);
                if (eaw.ShowDialog() != true)
                {
                    int index = IndeksAerodroma(oldAirport.Sifra);
                    Data.Instance.Aerodromi[index] = oldAirport;
                }
            }

        }

        private void BtnObrisiAerodrom_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Uspesno obrisano");
            Aerodrom selektovani = view.CurrentItem as Aerodrom;
            Data.Instance.Aerodromi.Remove(selektovani);
            Aerodrom.ObrisiAerodrom(selektovani);
        }

        private int IndeksAerodroma(String sifra)
        {
            int indeks = -1;
            for (int i=0; i<Data.Instance.Aerodromi.Count; i++)
            {
                if (Data.Instance.Aerodromi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Nije selektovan aerodrom! ");
                return false;
            }
            return true;
        }

        private void DGAerodromi_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Osvezi_Click(object sender, RoutedEventArgs e)
        {
            
            DGAerodromi.ItemsSource = null;
            view = null;

            view2 = CollectionViewSource.GetDefaultView(Data.Instance.Aerodromi);
            Aerodrom.UcitajAerodrome();
            DGAerodromi.ItemsSource = view2;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;

        }
    }
}
