﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
 
    public partial class Window1 : Window
    {
        ICollectionView view;

        public Window1()
        {
          
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Aviokompanije);
            DGAviokompanije.ItemsSource = view;
            DGAviokompanije.IsSynchronizedWithCurrentItem = true;

        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija aviokompanija = new Aviokompanija();
            AddEditAviokompanija eaw = new AddEditAviokompanija(new Aviokompanija(), AddEditAviokompanija.Opcija.DODAVANJE);
            eaw.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija selektovanaAviokompanija = (Aviokompanija)DGAviokompanije.SelectedItem;
            if (SelektovanaAviokompanija(selektovanaAviokompanija))
            {
                Aviokompanija stara = selektovanaAviokompanija.Clone() as Aviokompanija;
                AddEditAviokompanija eaw = new AddEditAviokompanija(selektovanaAviokompanija, AddEditAviokompanija.Opcija.IZMENA);
                if (eaw.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovaneAviokompanije(selektovanaAviokompanija.Sifra);
                    Data.Instance.Aviokompanije[indeks] = stara;
                }
            }
            DGAviokompanije.Items.Refresh();
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Uspesno obrisano");
            Aviokompanija selektovani = view.CurrentItem as Aviokompanija;
            Data.Instance.Aviokompanije.Remove(selektovani);
            Aviokompanija.ObrisiAviokompaniju(selektovani);
        }

        private bool SelektovanaAviokompanija(Aviokompanija aviokompanija)
        {
            if (aviokompanija == null)
            {
                MessageBox.Show("Nije selektovana aviokompanija");
                return false;
            }
            return true;
        }

        private int IndeksSelektovaneAviokompanije(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.Aviokompanije.Count; i++)
            {
                if (Data.Instance.Aviokompanije[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }
    }
}
