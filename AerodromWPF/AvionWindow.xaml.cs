﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AvionWindow.xaml
    /// </summary>
    public partial class AvionWindow : Window
    {
        ICollectionView view;

        public AvionWindow()
        {
          
            InitializeComponent();


            view = CollectionViewSource.GetDefaultView(Data.Instance.Avioni);
            DGavion.ItemsSource = view;
            DGavion.IsSynchronizedWithCurrentItem = true;


        }

        private void btnDodavanje_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = new Avion();
            AddEditAvion eaw = new AddEditAvion(avion, AddEditAvion.EOpcija.DODAVANJE);
            eaw.ShowDialog();


        }

        private void btnBrisanje_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Uspesno obrisano");
            Avion selektovani = view.CurrentItem as Avion;
            Data.Instance.Avioni.Remove(selektovani);
            Avion.ObrisiAvion(selektovani);
        }

        private void btnIzmena_Click(object sender, RoutedEventArgs e)
        {
            AddEditAvion eaw;
            Avion selectedAirport = (Avion)DGavion.SelectedItem;
            if (SelectedAvion(selectedAirport))
            {
                Avion oldAirport = (Avion)selectedAirport.Clone();
                eaw = new AddEditAvion(selectedAirport, AddEditAvion.EOpcija.IZMENA);
                if (eaw.ShowDialog() != true)
                {
                    int index = SelectedAvionIndex(oldAirport.Sifra);
                    Data.Instance.Avioni[index] = oldAirport;
                }
            }
        }
        private int SelectedAvionIndex(string sifra)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Avioni.Count; i++)
            {
                if (Data.Instance.Avioni[i].Sifra.Equals(sifra))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        private bool SelectedAvion(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Niste selektovali ni jedan avion.");
                return false;
            }
            return true;
        }

        private void DGAirports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
