﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AerodromWPF.Database
{
    class Data
    {
        public const string CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=AerodromiDB;Integrated Security=True";

        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Korisnik> Korisnici{ get; set; }
        public ObservableCollection<Aviokompanija> Aviokompanije { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }
        public ObservableCollection<Sediste> Sedista { get; set; }


        private Data()
        {
            Avioni = new ObservableCollection<Avion>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Korisnici = new ObservableCollection<Korisnik>();
            Aviokompanije = new ObservableCollection<Aviokompanija>();
            Karte = new ObservableCollection<Karta>();
            Sedista = new ObservableCollection<Sediste>();

        }

        private static Data _instance = null;

        public static Data Instance
        {

            get
            {
                if (_instance == null)
                    _instance = new Data();
                return _instance;
            }
        }


   


    }
}
