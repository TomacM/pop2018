﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static AerodromWPF.Model.Korisnik;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for DodavanjeBrisanjeKorisnika.xaml
    /// </summary>
    public partial class DodavanjeBrisanjeKorisnika : Window
    {
        public enum Option { DODAVANJE, IZMENA}
        Korisnik korisnik;
        Option option;


        public DodavanjeBrisanjeKorisnika(Korisnik korisnik,Option option)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.option = option;

            this.DataContext = korisnik;

            CbPol.ItemsSource = Enum.GetValues(typeof(EPol));
            CbTip.ItemsSource = Enum.GetValues(typeof(ETip));

            if (option.Equals(Option.IZMENA))
            {
                txtKorisnickoIme.IsEnabled = false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (option.Equals(Option.DODAVANJE))
            {
                foreach (var kor in Data.Instance.Korisnici)
                {
                    if (txtKorisnickoIme.Text.Equals(kor.KorisnickoIme))
                    {
                        MessageBox.Show("Vec postoji korisnik sa tim korisnickim imenom korisnicko ime mora biti jedinstveno");
                    }
                }
            }
            if (string.IsNullOrEmpty(CbTip.Text) || string.IsNullOrEmpty(CbPol.Text) || string.IsNullOrEmpty(txtAdresa.Text) || string.IsNullOrEmpty(txtIme.Text) || string.IsNullOrEmpty(txtKorisnickoIme.Text) || string.IsNullOrEmpty(txtPrezime.Text) || string.IsNullOrEmpty(txtSifra.Text))
            {
                MessageBox.Show("Niste popunili sva polja");
            }
            else { 
            if (!Validation.GetHasError(txtEmail))
                {
                this.DialogResult = true;

                    if (option.Equals(Option.DODAVANJE) && !AllreadyExistKorisnik(korisnik.KorisnickoIme))
                    {
                        if (option.Equals(Option.IZMENA))
                        {
                            MessageBox.Show("Uspesno sacuvano");
                            this.DialogResult = true;
                            Korisnik.IzmeniKorisnika(korisnik);

                            this.Close();
                        }
                        else if (option.Equals(Option.DODAVANJE))
                        {
                            Data.Instance.Korisnici.Add(korisnik);
                            MessageBox.Show("Uspesno sacuvano");
                            Korisnik.DodajKorisnika(korisnik);

                            this.Close();
                        }
                    }

                }
            }
        }
      
        private bool AllreadyExistKorisnik(string code)
        {
            foreach (var a in Data.Instance.Korisnici)
            {
                if (a.KorisnickoIme.Equals(code))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
