﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class EditOrAddLetWindow : Window
    {

        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Let let;

        public EditOrAddLetWindow(Let let,EOpcija opcija)
        {
            InitializeComponent();
            this.let = let;
            this.opcija = opcija;

            cbDestinacija.ItemsSource = Data.Instance.Aerodromi.Select(a => a.Sifra);
            CbOdrediste.ItemsSource = Data.Instance.Aerodromi.Select(a => a.Sifra);
            CbAviokompanija.ItemsSource = Data.Instance.Aviokompanije.Select(a => a.Sifra);


            this.DataContext = let;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtSifraLeta.IsEnabled = false;

            }
            TxtSifraLeta.Text = let.SifraLeta;
            TxtCenaKarte.Text = let.CenaKarte;
            cbDestinacija.SelectedItem = let.Destinacija;
            CbOdrediste.SelectedItem = let.Odrediste;
            CbAviokompanija.SelectedItem = let.AvioKompanija;
            VremeDolaska.Text = let.VremeSletanja;
            VremePolaska.Text = let.VremePoletanja;
            txtPilot.Text = let.Pilot;

        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        { 

            if (opcija.Equals(EOpcija.DODAVANJE))
            {
                foreach (var le in Data.Instance.Letovi)
                {
                    if (TxtSifraLeta.Text.Equals(le.SifraLeta))
                    {
                        MessageBox.Show("Vec postoji let sa tom sifrom sifra mora biti jedinstvena");
                    }
                }
            }
            if (string.IsNullOrEmpty(VremeDolaska.Text) || string.IsNullOrEmpty(VremePolaska.Text) || string.IsNullOrEmpty(TxtCenaKarte.Text) || string.IsNullOrEmpty(TxtSifraLeta.Text) || string.IsNullOrEmpty(txtPilot.Text) || string.IsNullOrEmpty(CbAviokompanija.Text) || string.IsNullOrEmpty(CbOdrediste.Text) || string.IsNullOrEmpty(CbAviokompanija.Text))
            {
                MessageBox.Show("Niste popunili sva polja");
            } else if (CbOdrediste.Text == cbDestinacija.Text)
            {
                MessageBox.Show("Odrediste i destinacija vam je ista");
            }
            else if (VremePolaska.Value > VremeDolaska.Value) {
                MessageBox.Show("Dolazak je pre polaska o.O");
            }
            
            else
            {
                if (opcija.Equals(EOpcija.IZMENA))
                {
                    MessageBox.Show("Uspesno sacuvano");
                    this.DialogResult = true;
                    Let.IzmeniLet(let);

                    this.Close();
                }
                else if (opcija.Equals(EOpcija.DODAVANJE))
                {
                    Data.Instance.Letovi.Add(let);
                    MessageBox.Show("Uspesno sacuvano");
                    this.DialogResult = true;
                    Let.DodajLet(let);

                    this.Close();
                }
            }
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        private bool AllreadyExistLet(string sifra)
        {
            foreach (var a in Data.Instance.Letovi)
            {
                if (a.SifraLeta.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
    }
}