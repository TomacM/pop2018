﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for KartaWindow.xaml
    /// </summary>
    public partial class KartaWindow : Window
    {
        public enum Option { DODAVANJE, IZMENA }
        Karta karta;
        Option option;

        ICollectionView view;

        public KartaWindow(Korisnik korisnik,Karta karta,Option option)
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Letovi);
            dgLetovi.ItemsSource = view;
            dgLetovi.IsSynchronizedWithCurrentItem = true;
            txtSifraLeta.IsEnabled = false;
            txtGateway.IsEnabled = false;
            txtGateway2.IsEnabled = false;
            txtSediste.IsEnabled = false;
            Red1.IsEnabled = false;
            Red2.IsEnabled = false;
            red3.IsEnabled = false;
            red4.IsEnabled = false;


            cbRed.Items.Add("1");
            cbRed.Items.Add("2");
            cbRed.Items.Add("3");
            cbRed.Items.Add("4");

            cbKolona.Items.Add("1");
            cbKolona.Items.Add("2");
            cbKolona.Items.Add("3");
            cbKolona.Items.Add("4");
            cbKolona.Items.Add("5");
            cbKolona.Items.Add("6");
            cbKolona.Items.Add("7");
            cbKolona.Items.Add("8");
            cbKolona.Items.Add("9");


            cbKlasaSedista.ItemsSource=Enum.GetValues(typeof(EKlasaSedista));

          

            this.karta = karta;
            this.option = option;

            this.DataContext = karta;

            txtSifraKarte.IsEnabled = false;
            txtCenaKarte.IsEnabled = false;

        }

        private void BtnDodajLet_Click(object sender, RoutedEventArgs e)
        {
            Let selektovani = view.CurrentItem as Let;
            txtSifraLeta.Text = selektovani.SifraLeta;
            txtCenaKarte.Text = selektovani.CenaKarte;
            Red1.Text = null;
            Red2.Text = null;
            red3.Text = null;
            red4.Text = null;


            int brojac = 0;

            foreach (var kartas in Data.Instance.Karte)
            {
                if (int.Parse(kartas.Sifra) > brojac)
                {
                    brojac = int.Parse(kartas.Sifra);
                }
                brojac = brojac + 1;


            }
            txtSifraKarte.Text = brojac.ToString().Trim();


            Random random = new Random();
            int randomNumber = random.Next(1, 7);

            for (int i = 1; i < 10; i++)
            {
                foreach (Karta kar in Data.Instance.Karte)
                {

                    if (!kar.OznakaSedista.Equals("1" + i.ToString()) && !kar.SifraLeta.Equals(txtSifraLeta.Text.Trim()))
                    {
                        Red1.AppendText("|1" + i.ToString() + "|" + "    ");

                    }
                    else if (kar.OznakaSedista.Equals("1" + i.ToString()) && kar.SifraLeta.Equals(txtSifraLeta.Text.Trim()))
                    {
                        Red1.AppendText("|X|" + "     ");
                        i++;

                    }


                }
            }
            for (int i = 1; i < 10; i++)
            {
                Red2.AppendText("|2" + i.ToString() + "|" + "   ");

            }
            for (int i = 1; i < 10; i++)
            {
                red3.AppendText("|3" + i.ToString() + "|" + "   ");

            }
            for (int i = 1; i < 10; i++)
            {
                red4.AppendText("|4" + i.ToString() + "|" + "   ");

            }






            txtGateway2.Text = randomNumber.ToString();

        }

        private int IndeksLeta(String sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].SifraLeta.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Nije selektovan let! ");
                return false;
            }
            return true;
        }

        private void BtnKarta_Click(object sender, RoutedEventArgs e)
        {
        }

        private void BtnRezervisiKartu_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtIme.Text) || string.IsNullOrEmpty(txtPrezime.Text) || string.IsNullOrEmpty(txtKorIme.Text) || string.IsNullOrEmpty(cbKlasaSedista.Text))
            {
                MessageBox.Show("Niste popunili sva polja");
            }
            else
            {
                if (option.Equals(Option.IZMENA))
                {
                    karta.ImePutnika = txtIme.Text;
                    karta.PrezimePutnika = txtPrezime.Text;
                    karta.KorisnickoIme = txtKorIme.Text;
                    karta.SifraLeta = txtSifraLeta.Text;
                    karta.Sifra = txtSifraKarte.Text;
                    karta.OznakaSedista = txtSediste.Text;
                    this.DialogResult = true;
                    Karta.IzmeniKartu(karta);

                    this.Close();
                }
                else if (option.Equals(Option.DODAVANJE))
                {

                        karta.ImePutnika = txtIme.Text;
                        karta.PrezimePutnika = txtPrezime.Text;
                        karta.KorisnickoIme = txtKorIme.Text;
                        karta.SifraLeta = txtSifraLeta.Text;
                        karta.Sifra = txtSifraKarte.Text;
                        karta.OznakaSedista = txtSediste.Text;
                        if (cbKlasaSedista.Equals(EKlasaSedista.EKONOMSKAKLASA))
                        {
                            karta.CenaKarte = txtCenaKarte.Text;
                        }
                        else
                        {
                            karta.CenaKarte = (1.5 * float.Parse(txtCenaKarte.Text)).ToString();
                        }
                        karta.Gateway = int.Parse(txtGateway2.Text);

                    if (!provera(karta.OznakaSedista,karta.SifraLeta))
                    {
                        Data.Instance.Karte.Add(karta);

                        this.DialogResult = true;
                        Karta.DodajKartu(karta);

                        MessageBox.Show("Uspesno ste rezervisali kartu");

                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Izabrano sediste je zauzeto");
                    }
                }

            }

        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Uspesno obrisano");
            Karta.ObrisiKartu(karta);

            this.Close();
        }

        private void BtnSed_Click(object sender, RoutedEventArgs e)
        {

            if (string.IsNullOrEmpty(txtSifraLeta.Text))
            {
                MessageBox.Show("Prvo morate uneti let");

            }
            else
            {

                if(string.IsNullOrEmpty(cbKolona.Text))
                {
                    MessageBox.Show("Niste uneli kolonu");
                }
                else if (string.IsNullOrEmpty(cbRed.Text))
                {
                    MessageBox.Show("Niste uneli red");
                }
                else
                {
                    txtSediste.Text = cbRed.Text + cbKolona.Text;
                }
            }

            
        }
        private bool provera(string sediste,string let)
        {
            foreach(Karta kar in Data.Instance.Karte)
            {
                if(kar.OznakaSedista.Equals(sediste)&& kar.SifraLeta.Equals(let))
                {
                    return true;
                }
                
            }
            return false;

        }


    }
}
