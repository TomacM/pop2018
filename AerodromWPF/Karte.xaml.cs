﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for Karte.xaml
    /// </summary>
    public partial class Karte : Window
    {
        ICollectionView view;
        public Karte(Korisnik korisnik)
        {
            
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Karte);
            DGKarte.ItemsSource = view;
            DGKarte.IsSynchronizedWithCurrentItem = true;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = new Korisnik();
            Karta karta = new Karta();
            // TODO populate airport data
            KartaWindow eaw = new KartaWindow(korisnik,karta, KartaWindow.Option.DODAVANJE);
            eaw.ShowDialog();
            eaw.BtnObrisi.IsEnabled = false;
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Uspesno obrisano");
            Karta selektovani = view.CurrentItem as Karta;
            Data.Instance.Karte.Remove(selektovani);
            Karta.ObrisiKartu(selektovani);

        }

        private int IndexKarte(string code)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Karte.Count; i++)
            {
                if (Data.Instance.Karte[i].Sifra.Equals(code))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        private bool SelektovanaKarta(Karta karta)
        {
            if (karta == null)
            {
                MessageBox.Show("Niste selektovali ni jednu kartu .");
                return false;
            }
            return true;
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            KartaWindow eaw;
            Karta selektovani = view.CurrentItem as Karta;
            Korisnik korisnik = new Korisnik();
          
            if (SelektovanaKarta(selektovani))
            {
                Karta stara = (Karta)selektovani.Clone();
                
                eaw = new KartaWindow(korisnik,selektovani, KartaWindow.Option.IZMENA);
                eaw.txtSediste.Text = selektovani.OznakaSedista;
                eaw.txtIme.Text = selektovani.ImePutnika;
                eaw.txtPrezime.Text = selektovani.PrezimePutnika;
                eaw.txtKorIme.Text = selektovani.KorisnickoIme;
                eaw.BtnObrisi.IsEnabled = false;
                if (eaw.ShowDialog() != true)
                {
                    
                    int index = IndexKarte(stara.Sifra);
                    Data.Instance.Karte[index] = stara;
                }
            }

        }
    }
}
