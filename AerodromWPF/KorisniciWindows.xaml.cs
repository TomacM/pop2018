﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{

    public partial class KorisniciWindows : Window
    {
        ICollectionView view;
        public KorisniciWindows()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Korisnici);
            DGkorisnici.ItemsSource = view;
            DGkorisnici.IsSynchronizedWithCurrentItem = true;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = new Korisnik();
            // TODO populate airport data
            DodavanjeBrisanjeKorisnika eaw = new DodavanjeBrisanjeKorisnika(korisnik, DodavanjeBrisanjeKorisnika.Option.DODAVANJE);
            eaw.ShowDialog();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            DodavanjeBrisanjeKorisnika eaw;
            Korisnik selectedKorisnik = (Korisnik)DGkorisnici.SelectedItem;
            if (SelectedKorisnik(selectedKorisnik))
            {
                Korisnik oldKorisnik = (Korisnik)selectedKorisnik.Clone();
                eaw = new DodavanjeBrisanjeKorisnika(selectedKorisnik, DodavanjeBrisanjeKorisnika.Option.IZMENA);
                if (eaw.ShowDialog() != true)
                {
                    int index = SelectedKorisnikIndex(oldKorisnik.KorisnickoIme);
                    Data.Instance.Korisnici[index] = oldKorisnik;
                }
            }
        }
        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Uspesno obrisano");
            Korisnik selektovani = view.CurrentItem as Korisnik;
            Data.Instance.Korisnici.Remove(selektovani);
            Korisnik.ObrisiKorisnika(selektovani);
        }

        private int SelectedKorisnikIndex(string code)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
            {
                if (Data.Instance.Korisnici[i].KorisnickoIme.Equals(code))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        private bool SelectedKorisnik(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Niste selektovali ni jednog korisnika .");
                return false;
            }
            return true;
        }

        private void TxtKor_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
