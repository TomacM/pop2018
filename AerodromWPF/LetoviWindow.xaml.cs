﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{

    public partial class LetoviWindow : Window
    {
        ICollectionView view;
        private ObservableCollection<Let> letovi1 { get; set; }
        public LetoviWindow()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Data.Instance.Letovi);
            DGLet.ItemsSource = view;
            DGLet.IsSynchronizedWithCurrentItem = true;

     

        }

        private void btnDodavanje_Click(object sender, RoutedEventArgs e)
        {
            Let let = new Let();
            EditOrAddLetWindow eaw = new EditOrAddLetWindow(let, EditOrAddLetWindow.EOpcija.DODAVANJE);
            eaw.ShowDialog();
        }

        private void btnIzmena_Click(object sender, RoutedEventArgs e)
        {
            EditOrAddLetWindow eaw;
            Let selectedLet = (Let)DGLet.SelectedItem;

      
            
            if (SelektovanLet(selectedLet))
            {
                Let oldLet = (Let)selectedLet.Clone();
      
                eaw = new EditOrAddLetWindow(selectedLet, EditOrAddLetWindow.EOpcija.IZMENA);
                eaw.txtPilot.Text=selectedLet.Pilot;
                eaw.CbOdrediste.SelectedIndex= eaw.CbOdrediste.Items.IndexOf(selectedLet.Odrediste);


                if (eaw.ShowDialog() != true)
                {
                    int index = IndeksLeta(oldLet.SifraLeta);
                    Data.Instance.Letovi[index] = oldLet;
                }
            }
        }

        private void btnBrisanje_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Uspesno obrisano");
            Let selektovani = view.CurrentItem as Let;
            Data.Instance.Letovi.Remove(selektovani);
            Let.ObrisiLet(selektovani);
        }

        private int IndeksLeta(String sifraLeta)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].SifraLeta.Equals(sifraLeta))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Nije selektovan let! ");
                return false;
            }
            return true;
        }

    }
}

