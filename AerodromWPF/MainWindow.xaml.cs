﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Korisnik korica;

        public MainWindow(Korisnik korisnik)
        {
            InitializeComponent();
            korica = korisnik;
            txtK.Text = korica.Ime + " " + korica.Prezime;
            txtK.IsEnabled = false;

        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            
            AerodromiWindow aerodromiWindow = new AerodromiWindow();
            aerodromiWindow.Show();
          
       


        }

        private void BtnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AvionWindow aw = new AvionWindow();
            aw.Show();
  
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            LetoviWindow l = new LetoviWindow();
            l.Show();
        }

        private void BtnAvioKompsnije_Click(object sender, RoutedEventArgs e)
        {
            Window1 b = new Window1();
            b.Show();

        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisniciWindows k = new KorisniciWindows();
            k.Show();
        }

        private void BtnOdjava_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult poruka = MessageBox.Show("Da li zelite da se odjavite", "Odjava", MessageBoxButton.YesNo,MessageBoxImage.Question);
            if (poruka== MessageBoxResult.Yes)
            {
                this.Close();

            }
       
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            Korisnik ko = new Korisnik();
            Karte k = new Karte(ko);
            k.Show();
        }
    }
}
