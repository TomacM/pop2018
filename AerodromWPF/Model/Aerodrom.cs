﻿using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Aerodrom
    {
        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; OnPropertyChanged("Naziv"); }
        }

        private string grad;

        public string Grad
        {
            get { return grad; }
            set { grad = value; OnPropertyChanged("Grad"); }
        }

        private bool deleted;

        public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; OnPropertyChanged("Deleted"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string naziv)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(naziv));
            }
        }

        public override string ToString()
        {
            return $"Sifra {Sifra} Naziv {Naziv} Grad {Grad}";
        }

        public object Clone()
        {
            Aerodrom newAirport = new Aerodrom
            {
                Sifra = this.Sifra,
                Naziv = this.Naziv,
                Grad = this.Grad,
                Deleted = this.Deleted
            };

            return newAirport;
        }

        public static void UcitajAerodrome()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aerodromiCommand = connection.CreateCommand();
                aerodromiCommand.CommandText = @"Select distinct * from aerodrom where deleted=1";
                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                daAerodromi.SelectCommand = aerodromiCommand;
                daAerodromi.Fill(ds, "aerodrom");

                foreach (DataRow row in ds.Tables["aerodrom"].Rows)
                {
                    Aerodrom a = new Aerodrom();
                    a.Sifra = (string)row["sifra"];
                    a.naziv = (string)row["nazivAerodroma"];
                    a.Grad = (string)row["Grad"];
                    a.deleted = (bool)row["Deleted"];

                    Data.Instance.Aerodromi.Add(a);
                }
            }
        }

        public static void DodajAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO aerodrom values(@sifra,@nazivAerodroma,@Grad,1)";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));
                command.Parameters.Add(new SqlParameter("@nazivAerodroma", a.naziv));
                command.Parameters.Add(new SqlParameter("@Grad", a.grad));
                command.Parameters.Add(new SqlParameter("@deleted", a.deleted));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
       
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"update aerodrom set deleted=0 where sifra=@sifra";

                    command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                    command.ExecuteNonQuery();
         
            }
        }

        public static void IzmeniAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE aerodrom SET Grad=@Grad, nazivAerodroma=@Naziv  WHERE sifra=@sifra";

                    command.Parameters.Add(new SqlParameter("@Naziv", a.naziv));
                    command.Parameters.Add(new SqlParameter("@Grad", a.grad));
                    command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                    command.ExecuteNonQuery();
            }
        }



    }
}