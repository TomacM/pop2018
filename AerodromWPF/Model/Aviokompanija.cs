﻿using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Aviokompanija : INotifyPropertyChanged, ICloneable
    {
        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; OnPropertyChanged("naziv"); }
        }

        private bool deleted;
        public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; OnPropertyChanged("Deleted"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string naziv)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(naziv));
            }
        }

        public override string ToString()
        {
            return $"Sifta {Sifra} Naziv {Naziv}";
        }

        public object Clone()
        {
            Aviokompanija nova = new Aviokompanija
            {
                Sifra = this.Sifra,
                Naziv = this.Naziv
            };
            return nova;
        }

        public static void UcitajAviokompanije()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aviokompanijeCommand = connection.CreateCommand();
                aviokompanijeCommand.CommandText = @"Select distinct * from avioKompanija where deleted=1";
                SqlDataAdapter daAviokompanije = new SqlDataAdapter();
                daAviokompanije.SelectCommand = aviokompanijeCommand;
                daAviokompanije.Fill(ds, "avioKompanija");

                foreach (DataRow row in ds.Tables["avioKompanija"].Rows)
                {
                    Aviokompanija a = new Aviokompanija();
                    a.sifra = (string)row["sifraAviokompanije"];
                    a.naziv = (string)row["nazivAviokompanije"];
                    a.deleted = (bool)row["Deleted"];

                    Data.Instance.Aviokompanije.Add(a);
                }
            }
        }

        public static void DodajAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO avioKompanija values(@sifra,@nazivAviokompanije,1)";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));
                command.Parameters.Add(new SqlParameter("@nazivAviokompanije", a.naziv));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update avioKompanija set deleted=0 where sifraAviokompanije=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniAviokompanije(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE avioKompanija SET nazivAviokompanije=@Naziv WHERE sifraAviokompanije=@sifra";

                command.Parameters.Add(new SqlParameter("@Naziv", a.naziv));
                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                command.ExecuteNonQuery();
            }
        }


    }
}
