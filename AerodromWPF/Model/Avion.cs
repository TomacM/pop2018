﻿using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Avion : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }


        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }
        private string sedistaBiz;

        public string SedistaBiz
        {
            get { return sedistaBiz; }
            set { sedistaBiz = value; OnPropertyChanged("SedistaBiz"); }
        }
        private string sedistaEko;

        public string SedistaEko
        {
            get { return sedistaEko; }
            set { sedistaEko = value; OnPropertyChanged("SedistaEko"); }
        }

        private string nazivAvioKompanije;

        public string NazivAvioKompanije
        {
            get { return nazivAvioKompanije; }
            set { nazivAvioKompanije = value; OnPropertyChanged("NazivAvioKompanije"); }
        }
        private bool deleted;
        public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; OnPropertyChanged("Deleted"); }
        }

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $" BrojLeta {BrojLeta} SedistaBiz{SedistaBiz} SedistaEko{SedistaEko} NazivAvioKompanije{NazivAvioKompanije}";
        }
        public object Clone()
        {
            Avion newAvion = new Avion
            {

                BrojLeta = this.BrojLeta,
                SedistaBiz = this.SedistaBiz,
                SedistaEko = this.SedistaEko,
                NazivAvioKompanije = this.NazivAvioKompanije,
                Deleted = this.Deleted
            };

            return newAvion;
        }

        public static void UcitajAvione()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand avionCommand = connection.CreateCommand();
                avionCommand.CommandText = @"Select distinct * from avion where deleted=1";
                SqlDataAdapter daAvioni = new SqlDataAdapter();
                daAvioni.SelectCommand = avionCommand;
                daAvioni.Fill(ds, "avion");

                foreach (DataRow row in ds.Tables["avion"].Rows)
                {
                    
                    Avion a = new Avion();

                    a.sifra =(string)row["sifra"];
                    a.brojLeta = (string)row["brojLeta"];
                    a.sedistaBiz = (string)row["BiznisKl"];
                    a.sedistaEko = (string)row["EkoKl"];
                    a.nazivAvioKompanije = (string)row["nazivAviokompanije"];
                    a.deleted = (bool)row["Deleted"];
                   

                    Data.Instance.Avioni.Add(a);
                }
            }
        }
        public static void DodajAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO avion values(@brojLeta,@BiznisKl,@EkoKl,@sifraAviokompanije,1,@sifra)";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));
                command.Parameters.Add(new SqlParameter("@brojLeta", a.brojLeta));
                command.Parameters.Add(new SqlParameter("@BiznisKl", a.sedistaBiz));
                command.Parameters.Add(new SqlParameter("@EkoKl", a.sedistaEko));
                command.Parameters.Add(new SqlParameter("@deleted", a.deleted));
                command.Parameters.Add(new SqlParameter("@sifraAviokompanije", a.nazivAvioKompanije));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update avion set deleted=0 where sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE avion SET brojLeta=@brojLeta, BiznisKl=@BiznisKl,EkoKl=@EkoKl,nazivAviokompanije=@nazivAviokompanije WHERE sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@brojLeta", a.brojLeta));
                command.Parameters.Add(new SqlParameter("@BiznisKl", a.sedistaBiz));
                command.Parameters.Add(new SqlParameter("@EkoKl", a.sedistaEko));
                command.Parameters.Add(new SqlParameter("@nazivAviokompanije", a.nazivAvioKompanije));
                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                command.ExecuteNonQuery();
            }
        }
    }
}
