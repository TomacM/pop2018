﻿using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Karta : INotifyPropertyChanged, ICloneable
    {

        private string sifraLeta;
        public string SifraLeta
        {
            get { return sifraLeta; }
            set { sifraLeta = value; OnPropertyChanged("SifraLeta"); }
        }

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; }
        }


        private string oznakaSedista;
        public string OznakaSedista
        {
            get { return oznakaSedista; }
            set { oznakaSedista = value; OnPropertyChanged("OznakaSedista"); }
        }


        private string imePutnika;
        public string ImePutnika
        {
            get { return imePutnika; }
            set { imePutnika = value; OnPropertyChanged("ImePutnika"); }
        }
        private string prezimePutnika;
        public string PrezimePutnika
        {
            get { return prezimePutnika; }
            set { prezimePutnika = value; OnPropertyChanged("PrezimePutnika"); }
        }

        private string korisnickoIme;
        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme= value; OnPropertyChanged("KorisnickoIme"); }
        }

        private int gateway;
        public int Gateway
        {
            get { return gateway; }
            set { gateway = value; OnPropertyChanged("Gateway"); }
        }

        private EKlasaSedista klasaSedista;
        public EKlasaSedista KlasaSedista
        {
            get { return klasaSedista; }
            set { klasaSedista = value; }
        }



        private string cenaKarte;
        public string CenaKarte
        {
            get { return cenaKarte; }
            set { cenaKarte = value; OnPropertyChanged("CenaKarte"); }
        }

        private bool deleted;
        public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; OnPropertyChanged("Obrisano"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            Karta nova = new Karta
            {
                SifraLeta = this.SifraLeta,
                OznakaSedista = this.OznakaSedista,
                ImePutnika = this.ImePutnika,
                PrezimePutnika = this.PrezimePutnika,
                KorisnickoIme = this.KorisnickoIme,
                KlasaSedista = this.KlasaSedista,
                CenaKarte = this.CenaKarte,
                Sifra = this.Sifra,
                Deleted = this.deleted,
                Gateway=this.gateway
            };
            return nova;
        }

        public static void UcitajKarte()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand karteCommand = connection.CreateCommand();
                karteCommand.CommandText = @"Select distinct * from karta where deleted=1";
                SqlDataAdapter daKarte = new SqlDataAdapter();
                daKarte.SelectCommand = karteCommand;
                daKarte.Fill(ds, "karta");

                foreach (DataRow row in ds.Tables["karta"].Rows)
                {
                    Karta a = new Karta();
                    a.sifraLeta = (string)row["sifraLeta"];
                    a.sifra = (string)row["sifraKarte"];
                    a.oznakaSedista = (string)row["oznakaSedista"];
                    a.imePutnika = (string)row["imePutnika"];
                    a.prezimePutnika = (string)row["prezimePutnika"];
                    a.klasaSedista = (EKlasaSedista)row["klasaSedista"];
                    a.cenaKarte = (string)row["cenaKarte"];
                    a.korisnickoIme = (string)row["korisnickoIme"];
                    a.gateway = (int)row["gateway"];
                    a.deleted = (bool)row["Deleted"];

                    Data.Instance.Karte.Add(a);
                }
            }
        }

        public static void DodajKartu(Karta a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO karta values(@sifraleta,@sifraKarte,@oznakaSedista,@imePutnika,@prezimePutnika,@klasaSedista,@cenaKarte,1,@korisnickoIme,@gateway)";

                command.Parameters.Add(new SqlParameter("@sifraleta", a.sifraLeta));
                command.Parameters.Add(new SqlParameter("@oznakaSedista", a.oznakaSedista));
                command.Parameters.Add(new SqlParameter("@sifraKarte", a.sifra));
                command.Parameters.Add(new SqlParameter("@imePutnika", a.imePutnika));
                command.Parameters.Add(new SqlParameter("@prezimePutnika", a.prezimePutnika));
                command.Parameters.Add(new SqlParameter("@klasaSedista", a.klasaSedista));
                command.Parameters.Add(new SqlParameter("@cenaKarte", a.cenaKarte));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korisnickoIme));
                command.Parameters.Add(new SqlParameter("@gateway", a.gateway));
                command.Parameters.Add(new SqlParameter("@deleted", a.deleted));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiKartu(Karta a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update karta set deleted=0 where sifraKarte=@sifra";
                

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));



                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniKartu(Karta a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE karta SET gateway=@gateway, sifraLeta=@sifraLeta,oznakaSedista=@oznakaSedista,imePutnika=@imePutnika,prezimePutnika=@prezimePutnika,klasaSedista=@klasaSedista,cenaKarte=@cenaKarte,korisnickoIme=@korisnickoIme WHERE sifraKarte=@sifraKarte";

                command.Parameters.Add(new SqlParameter("@sifraLeta", a.sifraLeta));              
                command.Parameters.Add(new SqlParameter("@oznakaSedista", a.oznakaSedista));
                command.Parameters.Add(new SqlParameter("@sifraKarte", a.sifra));
                command.Parameters.Add(new SqlParameter("@imePutnika", a.imePutnika));
                command.Parameters.Add(new SqlParameter("@prezimePutnika", a.prezimePutnika));
                command.Parameters.Add(new SqlParameter("@klasaSedista", a.klasaSedista));
                command.Parameters.Add(new SqlParameter("@cenaKarte", a.cenaKarte));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korisnickoIme));
                command.Parameters.Add(new SqlParameter("@gateway", a.gateway));
                command.Parameters.Add(new SqlParameter("@deleted", a.deleted));

                command.ExecuteNonQuery();
            }
        }

        public static implicit operator Karta(Aviokompanija v)
        {
            throw new NotImplementedException();
        }
    }
}
