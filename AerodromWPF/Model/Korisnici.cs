﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Korisnici : INotifyPropertyChanged, ICloneable
    {
        enum Pol
        {
            MUSKO,
            ZENSKO
        }
        enum TipKorisnika
        {
            ADMIN,
            PUTNIK
        }
        public event PropertyChangedEventHandler PropertyChanged;


        private string ime;

        public string Ime
        {
            get { return ime; }
            set { ime = value; OnPropertyChanged("Ime"); }
        }

        private string prezime;

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; OnPropertyChanged("Prezime"); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }

        private string adresa;

        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; OnPropertyChanged("Adresa"); }
        }
        private string korisnickoIme;

        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnPropertyChanged("KorisnickoIme"); }
        }
        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private bool deleted;
        public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; OnPropertyChanged("Deleted"); }
        }

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Ime {Ime} Prezime {Prezime} Email {Email} Adresa {Adresa}";
        }
        public object Clone()
        {
            Korisnici newUser = new Korisnici
            {
                Ime = this.Ime,
                Prezime = this.Prezime,
                Email = this.Email,
                Adresa = this.Adresa,
                Deleted = this.Deleted
            };

            return newUser;
        }
    }
}

