﻿using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Korisnik : INotifyPropertyChanged, ICloneable
    {

   

        private EPol pol;
        public EPol Pol
        {
            get { return pol; }
            set { pol = value; }
        }

        private ETip tip;
        public ETip Tip
        {
            get { return tip; }
            set { tip = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private string ime;

        public string Ime
        {
            get { return ime; }
            set { ime = value; OnPropertyChanged("Ime"); }
        }


        private string prezime;

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; OnPropertyChanged("Prezime"); }
        }


        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }


        private string adresa;

        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; OnPropertyChanged("Adresa"); }
        }


        private string korisnickoIme;

        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnPropertyChanged("KorisnickoIme"); }
        }


        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }



        private bool deleted;
            public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; OnPropertyChanged("Deleted"); }
        }



 


        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            Korisnik novi = new Korisnik
            {
                Ime = this.Ime,
                Prezime = this.Prezime,
                Email = this.Email,
                Adresa = this.Adresa,
                Pol = this.Pol,
                KorisnickoIme=this.KorisnickoIme,
                Sifra = this.Sifra,
                Tip = this.Tip
            };

            return novi;
        }
        public override string ToString()
        {
            return $"{Ime} {Prezime} {Adresa} {Email} {Pol} {KorisnickoIme} {Sifra} {Tip}";
        }

        public static void UcitajKorisnike()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand korisnikCommand = connection.CreateCommand();
                korisnikCommand.CommandText = @"Select distinct * from korisnik where deleted=1";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                daKorisnici.SelectCommand = korisnikCommand;
                daKorisnici.Fill(ds, "korisnik");

                foreach (DataRow row in ds.Tables["korisnik"].Rows)
                {
                    Korisnik k = new Korisnik();
                    k.pol = (EPol)row["pol"];
                    k.ime = (string)row["ime"];
                    k.prezime = (string)row["prezime"];
                    k.email = (string)row["email"];
                    k.adresa = (string)row["adresa"];
                    k.korisnickoIme = (string)row["korisnickoIme"];
                    k.sifra = (string)row["lozinka"];
                    k.tip = (ETip)row["tipKorisnika"];
                    k.deleted= (bool)row["Deleted"];

                    Data.Instance.Korisnici.Add(k);
                }
            }
        }

        public static void DodajKorisnika(Korisnik a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO korisnik values(@ime,@prezime,@email,@adresa,@pol,@korisnickoIme,@lozinka,@tipKorisnika,1)";

                command.Parameters.Add(new SqlParameter("@ime", a.ime));
                command.Parameters.Add(new SqlParameter("@prezime", a.prezime));
                command.Parameters.Add(new SqlParameter("@email", a.email));
                command.Parameters.Add(new SqlParameter("@adresa", a.adresa));
                command.Parameters.Add(new SqlParameter("@pol", a.pol));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", a.sifra));
                command.Parameters.Add(new SqlParameter("@tipKorisnika", a.tip));
                command.Parameters.Add(new SqlParameter("@deleted", a.deleted));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiKorisnika(Korisnik a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update korisnik set deleted=0 where korisnickoIme=@korisnickoIme";

                command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korisnickoIme));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniKorisnika(Korisnik a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE korisnik SET ime=@ime, prezime=@prezime,email=@email,adresa=@adresa,pol=@pol,lozinka=@lozinka,tipKorisnika=@tipKorisnika WHERE korisnickoIme=@korisnickoIme";

                command.Parameters.Add(new SqlParameter("@ime", a.ime));
                command.Parameters.Add(new SqlParameter("@prezime", a.prezime));
                command.Parameters.Add(new SqlParameter("@email", a.email));
                command.Parameters.Add(new SqlParameter("@adresa", a.adresa));
                command.Parameters.Add(new SqlParameter("@pol", a.pol));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", a.sifra));
                command.Parameters.Add(new SqlParameter("@tipKorisnika", a.tip));
                command.Parameters.Add(new SqlParameter("@deleted", a.deleted));

                command.ExecuteNonQuery();
            }
        }
    }
}


