﻿using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Let : INotifyPropertyChanged, ICloneable
    {
        private string pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; OnPropertyChanged("Pilot"); }
        }
        private string sifraLeta;

        public string SifraLeta
        {
            get { return sifraLeta; }
            set { sifraLeta = value; OnPropertyChanged("SifraLeta"); }
        }

        private string destinacija;

        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnPropertyChanged("Destinacija"); }
        }

        private string odrediste;

        public string Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; OnPropertyChanged("Odrediste"); }
        }

        private string cenaKarte;

        public string CenaKarte
        {
            get { return cenaKarte; }
            set { cenaKarte = value; OnPropertyChanged("CenaKarte"); }
        }


        private string vremePoletanja;

        public string VremePoletanja
        {
            get { return vremePoletanja; }
            set { vremePoletanja= value; OnPropertyChanged("VremePoletanja"); }
        }

        private string vremeSletanja;

        public string VremeSletanja
        {
            get { return vremeSletanja; }
            set { vremeSletanja = value; OnPropertyChanged("VremeSletanja"); }
        }

        private bool deleted;
        public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; OnPropertyChanged("Deleted"); }
        }

        private string avioKompanija;

        public string AvioKompanija
        {
            get { return avioKompanija; }
            set { avioKompanija = value; OnPropertyChanged("AvioKompanija"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string naziv)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(naziv));
            }
        }

        public override string ToString()
        {
            return $"Sifta {SifraLeta} Destinacija {Destinacija} Odrediste {Odrediste} Cena karte {cenaKarte} Vreme polaska {VremePoletanja} Vreme dolaska {vremeSletanja}";
        }

        public object Clone()
        {
            return new Let
            {
                SifraLeta = this.SifraLeta,
                Destinacija = this.Destinacija,
                Odrediste = this.Odrediste,
                CenaKarte = this.CenaKarte,
                VremePoletanja = this.VremePoletanja,
                VremeSletanja = this.VremeSletanja
            };
        }

        public static void UcitajLetove()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand letCommand = connection.CreateCommand();
                letCommand.CommandText = @"Select distinct * from let where deleted=1";
                SqlDataAdapter dalet = new SqlDataAdapter();
                dalet.SelectCommand = letCommand;
                dalet.Fill(ds, "let");

                foreach (DataRow row in ds.Tables["let"].Rows)
                {
                    Let l = new Let();
                    l.sifraLeta = (string)row["brojLeta"];
                    l.pilot = (string)row["pilot"];
                    l.vremePoletanja = (string)row["VremePolaska"];
                    l.VremeSletanja = (string)row["VremeDolaska"];
                    l.cenaKarte = (string)row["cenaKarte"];
                    l.destinacija = (string)row["Destinacija"];
                    l.odrediste = (string)row["Odrediste"];
                    l.avioKompanija = (string)row["aviokompanija"];
                    l.deleted = (bool)row["Deleted"];

                    Data.Instance.Letovi.Add(l);
                }
            }
        }

        public static void DodajLet(Let a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO let values(@pilot,@brojLeta,@VremePolaska,@VremeDolaska,@cenaKarte,@Destinacija,@Odrediste,1,@avioKompanija)";

                command.Parameters.Add(new SqlParameter("@pilot", a.pilot));
                command.Parameters.Add(new SqlParameter("@brojLeta", a.SifraLeta));
                command.Parameters.Add(new SqlParameter("@VremePolaska", a.vremePoletanja));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", a.vremeSletanja));
                command.Parameters.Add(new SqlParameter("@cenaKarte", a.cenaKarte));
                command.Parameters.Add(new SqlParameter("@Destinacija", a.destinacija));
                command.Parameters.Add(new SqlParameter("@Odrediste", a.odrediste));
                command.Parameters.Add(new SqlParameter("@avioKompanija", a.avioKompanija));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiLet(Let a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update let set deleted=0 where brojLeta=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifraLeta));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniLet(Let a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE let SET pilot=@pilot,VremePolaska=@VremePolaska,VremeDolaska=@VremeDolaska,cenaKarte=@cenaKarte,Destinacija=@Destinacija,Odrediste=@Odrediste,aviokompanija=@avioKompanija where brojLeta=@sifraLeta";

                command.Parameters.Add(new SqlParameter("@pilot", a.pilot));
                command.Parameters.Add(new SqlParameter("@sifraLeta", a.sifraLeta));
                command.Parameters.Add(new SqlParameter("@VremePolaska", a.vremePoletanja));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", a.vremeSletanja));
                command.Parameters.Add(new SqlParameter("@cenaKarte", a.cenaKarte));
                command.Parameters.Add(new SqlParameter("@Destinacija", a.destinacija));
                command.Parameters.Add(new SqlParameter("@Odrediste", a.odrediste));
                command.Parameters.Add(new SqlParameter("@avioKompanija", a.avioKompanija));


                command.ExecuteNonQuery();
            }
        }

    }
}
