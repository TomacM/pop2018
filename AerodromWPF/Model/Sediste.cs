﻿using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Sediste
    {
        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }

        private string brojSedista;

        public string BrojSedista
        {
            get { return brojSedista; }
            set { brojSedista = value; OnPropertyChanged("BrojSedista"); }
        }

        private string sifraKarte;

        public string SifraKarte
        {
            get { return sifraKarte; }
            set { sifraKarte = value; OnPropertyChanged("SifraKarte"); }
        }


        private EKlasaSedista klasa;
        public EKlasaSedista Klasa
        {
            get { return klasa; }
            set { klasa = value; }
        }

        public PropertyChangedEventHandler PropertyChanged { get; private set; }

        private void OnPropertyChanged(string v)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(v));
            }
        }
        public static void UcitajSedista()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand sedistaCommand = connection.CreateCommand();
                sedistaCommand.CommandText = @"Select distinct * from sediste where deleted=1";
                SqlDataAdapter daSedista = new SqlDataAdapter();
                daSedista.SelectCommand = sedistaCommand;
                daSedista.Fill(ds, "sediste");

                foreach (DataRow row in ds.Tables["sediste"].Rows)
                {
                    Sediste a = new Sediste();
                    a.brojLeta = (string)row["brojLeta"];
                    a.brojSedista = (string)row["brojSedista"];
                    a.Klasa = (EKlasaSedista)row["klasaSedista"];
                    

                    Data.Instance.Sedista.Add(a);
                }
            }
        }

        public static void DodajSedista(Sediste a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO sediste values(@brojSedista,@brojLeta,@klasaSedista)";

                command.Parameters.Add(new SqlParameter("@brojSedista", a.brojSedista));
                command.Parameters.Add(new SqlParameter("@brojLeta", a.brojLeta));
                command.Parameters.Add(new SqlParameter("@klasaSedista", a.Klasa));
              

                command.ExecuteNonQuery();


            }

        }
        public static void ObrisiSediste(Sediste a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"delete sediste where brojLeta=@brojLeta and sifraKarte=@sifra and oznakaSedista=@oznakaSedista";


                command.Parameters.Add(new SqlParameter("@sifra", a.sifraKarte));
                command.Parameters.Add(new SqlParameter("@brojLeta", a.brojLeta));
                command.Parameters.Add(new SqlParameter("@oznakaSedista", a.brojSedista));
                command.ExecuteNonQuery();

            }
        }
        public static void IzmeniSediste(Sediste a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE sediste SET brojLeta=@brojLeta, brojSedista=@brojSedista klasaSedista=@klasaSedista sifraKarte=@sifraKarte  WHERE brojLeta=@brojLeta and brojSedista=@brojSedista";

                command.Parameters.Add(new SqlParameter("@brojLeta", a.brojLeta));
                command.Parameters.Add(new SqlParameter("@brojSedista", a.brojSedista));
                command.Parameters.Add(new SqlParameter("@sifraKarte", a.sifraKarte));
                command.Parameters.Add(new SqlParameter("@klasaSedista", a.klasa));

                command.ExecuteNonQuery();
            }
        }
    }
}
