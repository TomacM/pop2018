﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for Prijava.xaml
    /// </summary>
    public partial class Prijava : Window
    {
        public Prijava()
        {
            InitializeComponent();
            Aerodrom.UcitajAerodrome();
            Aviokompanija.UcitajAviokompanije();
            Avion.UcitajAvione();
            Let.UcitajLetove();
            Korisnik.UcitajKorisnike();
            Karta.UcitajKarte();
          

    
        }

        private void BtnRegistracija_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = new Korisnik();
            korisnik.Tip = ETip.PUTNIK;
            
            DodavanjeBrisanjeKorisnika eaw = new DodavanjeBrisanjeKorisnika(korisnik, DodavanjeBrisanjeKorisnika.Option.DODAVANJE);
            eaw.CbTip.IsEnabled = false;
            eaw.ShowDialog();
            
            
        }

        private void Prikaz_Letaova_Click(object sender, RoutedEventArgs e)
        {
            LetoviWindow let = new LetoviWindow();
            let.btnBrisanje.IsEnabled = false;
            let.btnDodavanje.IsEnabled = false;
            let.btnIzmena.IsEnabled = false;
            let.ShowDialog();
        }

        private void BtnPrijava_Click(object sender, RoutedEventArgs e)
        {

            if (string.IsNullOrEmpty(txtKorIme.Text) || string.IsNullOrEmpty(txtSifra.Text))
            {
                MessageBox.Show("Niste popunili sva polja");
            }
            else
            {
                Korisnik korisnik = login(txtKorIme.Text.Trim(), txtSifra.Text.Trim());
                if (korisnik != null)
                {
                    txtKorIme.Text = "";
                    txtSifra.Text = "";
                    if (korisnik.Tip.Equals(ETip.ADMINISTRATOR))
                    {
                        MainWindow main = new MainWindow(korisnik);
                        main.ShowDialog();
                    }
                    else if (korisnik.Tip.Equals(ETip.PUTNIK))
                    {
                        PutnikProzor pu = new PutnikProzor(korisnik);
                        pu.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Pogresna prijava");
                }
                
            }


        }

        public Korisnik login(String korime, String sifra)
        {
            foreach (var korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(korime) && korisnik.Sifra.Equals(sifra))
                {
                    return korisnik;
                }
            }
            return null;
        }



    }
}
