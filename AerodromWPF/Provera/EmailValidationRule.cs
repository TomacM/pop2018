﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AerodromWPF.Provera
{
    class EmailValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value.ToString().Contains("@") && value.ToString().EndsWith(".com"))
            {
                return new ValidationResult(true, "Dobar email format");

            }

            return new ValidationResult(false, "Email mora da sadrzi @ i zavrsava sa .com");
        }
    }
}
