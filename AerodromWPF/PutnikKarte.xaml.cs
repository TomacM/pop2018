﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for PutnikKarte.xaml
    /// </summary>
    public partial class PutnikKarte : Window
    {
        Korisnik korica;
        public PutnikKarte(Korisnik korisnik)
        {
            InitializeComponent();
            korica = korisnik;

        

        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = new Karta();
            KartaWindow kar = new KartaWindow(korica, karta, KartaWindow.Option.DODAVANJE);

            kar.txtIme.Text = korica.Ime;
            kar.txtPrezime.Text = korica.Prezime;
            kar.txtKorIme.Text = korica.KorisnickoIme;
            kar.txtIme.IsEnabled = false;
            kar.txtPrezime.IsEnabled = false;
            kar.txtKorIme.IsEnabled = false;

            kar.ShowDialog();


        }

        private void ListaLetova_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string curItem = listaLetova.SelectedItem.ToString();

            foreach(var k in Data.Instance.Karte)
            {
                if (k.Sifra.Equals(curItem))
                {
                    KartaWindow kw = new KartaWindow(korica, k, KartaWindow.Option.IZMENA);

                    kw.txtSediste.Text = k.OznakaSedista;
                    kw.txtIme.Text = k.ImePutnika;
                    kw.txtPrezime.Text = k.PrezimePutnika;
                    kw.txtKorIme.Text = k.KorisnickoIme;

                    kw.txtIme.IsEnabled = false;
                    kw.txtPrezime.IsEnabled = false;
                    kw.txtKorIme.IsEnabled = false;

                    kw.ShowDialog();
                }
            }
        }
    }
}
