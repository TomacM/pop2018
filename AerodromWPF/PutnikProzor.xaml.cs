﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for PutnikProzor.xaml
    /// </summary>
    public partial class PutnikProzor : Window
    {
        Korisnik korica;
        

        public PutnikProzor(Korisnik korisnik)
        {
            InitializeComponent();
            this.korica = korisnik;

            txtW.Text = korica.Ime+" "+korica.Prezime;
            txtW.IsEnabled = false;

        }

        private void BtnProfil_Click(object sender, RoutedEventArgs e)
        {
        
            DodavanjeBrisanjeKorisnika eaw = new DodavanjeBrisanjeKorisnika(korica, DodavanjeBrisanjeKorisnika.Option.DODAVANJE);
            eaw.CbTip.IsEnabled = false;
            eaw.ShowDialog();
        }

        private void Btnodjava_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult poruka = MessageBox.Show("Da li zelite da se odjavite", "Odjava", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (poruka == MessageBoxResult.Yes)
            {
                this.Close();

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LetoviWindow lw = new LetoviWindow();
            lw.btnBrisanje.IsEnabled = false;
            lw.btnDodavanje.IsEnabled = false;
            lw.btnIzmena.IsEnabled = false;
            lw.Show();
           
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            
            PutnikKarte kar = new PutnikKarte(korica);

            foreach (var karte in Data.Instance.Karte)
            {
                if (karte.KorisnickoIme.Equals(korica.KorisnickoIme) && karte.Deleted.Equals(true))
                {
                    kar.listaLetova.Items.Add(karte.Sifra);
                }
            }
            kar.ShowDialog();

        }
    }
}
