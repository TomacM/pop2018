﻿#pragma checksum "..\..\PutnikProzor.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "D7B204ECB9B0C608EA6A86700EE58F023A86EF90"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AerodromWPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AerodromWPF {
    
    
    /// <summary>
    /// PutnikProzor
    /// </summary>
    public partial class PutnikProzor : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 28 "..\..\PutnikProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnProfil;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\PutnikProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnKarte;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\PutnikProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtW;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\PutnikProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnodjava;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AerodromWPF;component/putnikprozor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\PutnikProzor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnProfil = ((System.Windows.Controls.Button)(target));
            
            #line 28 "..\..\PutnikProzor.xaml"
            this.btnProfil.Click += new System.Windows.RoutedEventHandler(this.BtnProfil_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnKarte = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\PutnikProzor.xaml"
            this.btnKarte.Click += new System.Windows.RoutedEventHandler(this.BtnKarte_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 58 "..\..\PutnikProzor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.txtW = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.btnodjava = ((System.Windows.Controls.Button)(target));
            
            #line 75 "..\..\PutnikProzor.xaml"
            this.btnodjava.Click += new System.Windows.RoutedEventHandler(this.Btnodjava_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

